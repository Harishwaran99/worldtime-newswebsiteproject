console.log('Hii this is my website');

let apiKey = 'eb5039b9e1214d919d518a610a610a54';

//Grab the News container
let newsAccordion = document.getElementById('newsAccordion');
//Create an ajax get request
const xhr = new XMLHttpRequest();
xhr.open('GET',`https://newsapi.org/v2/top-headlines?country=us&category=sports&apiKey=${apiKey}`,true);

xhr.onload = function () {
    if (this.status === 200) {
        let json = JSON.parse(this.responseText);
        let articles = json.articles;
        console.log(articles);
        let newsHtml = "";
        articles.forEach(function(element, index) {
            // console.log(element, index)
            let news = `<div class="card mb-3 border-4" style="background-color:tomato;">
            <img class="card-img-top" src="${element['urlToImage']}" alt="Card image">
            <div class="card-body">
              <h5 class="card-title" style="color:white;">${element['title']}</h5>
              <p class="card-text">${element['description']}</p>
              <p class="card-text"><small>${element['publishedAt']}<strong></strong></small></p>
              <a href = "${element['url']}" target="_blank"style="color:#032a63; background-color:yellow;">Read More <span>>></span></a>
            </div>
          </div>`;
            newsHtml += news;
        });
        newsAccordion.innerHTML = newsHtml;
    }
    else {
        console.log("Some error occured")
    }
}

xhr.send()