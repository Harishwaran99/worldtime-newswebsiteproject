console.log('Hii this is my website');

let apiKey = 'eb5039b9e1214d919d518a610a610a54';

//Grab the News container
let newsAccordion = document.getElementById('newsAccordion');
//Create an ajax get request
const xhr = new XMLHttpRequest();
xhr.open('GET',`https://newsapi.org/v2/top-headlines?country=in&category=entertainment&apiKey=${apiKey}`,true);


// What to do when response is ready
xhr.onload = function () {
    if (this.status === 200) {
        let json = JSON.parse(this.responseText);
        let articles = json.articles;
        console.log(articles);
        let newsHtml = "";
        articles.forEach(function(element, index) {
            // console.log(element, index)
            let news = `<div class="card mb-3" style="max-width: 1100px; background-color:tomato; border-width: 10px;">
            <div class="row g-0"><div class="col-md-4">
            <img src="${element['urlToImage']}" class="img-fluid rounded-start" alt="cardimage">
            </div>
            <div class="col-md-8">
            <div class="card-body">
            <h5 class="card-title"><a href="${element['url']}" target="_blank" style="background-color:yellow; color:blue;">${element['title']}</a></h5>
            <p class="card-text">${element['description']}</p>
            <small class="text-muted">${element['publishedAt']}</small></p>
            </div>
            </div>
            </div>
            </div>`;
            newsHtml += news;
        });
        newsAccordion.innerHTML = newsHtml;
    }
    else {
        console.log("Some error occured")
    }
}

xhr.send()